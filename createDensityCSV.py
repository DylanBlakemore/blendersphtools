import numpy as np

from builds import sphdecode
from builds import sphdensity
from builds import sphvelocity

if __name__=="__main__":
    file_in  = "/home/dylan/Google Drive/PhD/blender/HorizontalChannel/cache_001(02)/456D6974746572_001000_00.bphys"
    file_out = ""
    
    bounds = [-1,1, \
              -0.25,0.25, \
              -0.25,0.25]
    
    m = 0.008
    h = 0.04
    rho0 = 1000
    
    print("Extracting data")
    [title, ptype, n, d, pid, x, y, z, vx, vy, vz] = sphdecode.decodeBlenderFile(file_in, bounds)
    combined = np.transpose(np.array([pid, x, y, z, vx, vy, vz]))
    print("Calculating density")
    rho = sphdensity.calculateDensity(m,h,rho0,combined[:,1:4])
    print("Calculating velocities")
    [vx_a, vy_a, vz_a] = sphvelocity.calculateVelocity(m,h,combined[:,1:4], combined[:,4:7], np.array(rho))
    
    print(vx_a)
    

    
    