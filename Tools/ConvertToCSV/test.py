import sphdecode
import sphdensity
import numpy as np

m = 0.012
h = 0.04
rho0 = 1000

filepath_in = '/home/dylan/Blender/NoSlipAnalysis/custom/noslip_fulldamp/456D6974746572_002000_00.bphys'
filepath_out = '/home/dylan/Blender/NoSlipAnalysis/custom/analysis/noslip_fulldamp/2000.csv'
bounds = [0.1,2.3,-0.205, 0.205, -0.205, 0.205]
[title, ptype, n, d, pid, x, y, z, vx, vy, vz] = sphdecode.decodeBlenderFile(filepath_in, bounds)
inds = np.where(np.logical_and(np.asarray(pid) > -1, ~np.isnan(np.asarray(x))))[0]
     
combined = np.transpose(np.array([pid, x, y, z, vx, vy, vz]))
combined_valid = combined[inds,:]
rho = sphdensity.calculateDensity(m,h,combined_valid[:,1:4])
outarray = np.ndarray((combined_valid.shape[0],10))
outarray[:,0:7] = combined_valid
outarray[:,7] = np.reshape(rho, (len(rho),))
outarray[:,8] = m
outarray[:,9] = h
fileheader = 'ID,X,Y,Z,Vx,Vy,Vz,rho,pmass,h'
np.savetxt(filepath_out, outarray, delimiter=',',header=fileheader,comments='')

#==============================================================================
# for i in range(400,500):
#     filepath_in = prefix + str(i) + '_00.bphys'
#     filepath_out = '/home/dylan/Blender/UpperAirway_02/bc_01_analysis/' + str(i) + '.csv'
#     [title, ptype, n, d, pid, x, y, z, vx, vy, vz] = sphdecode.decodeBlenderFile(filepath_in, bounds, 20)
#     
#     inds = np.where(np.logical_and(np.asarray(pid) > -1, ~np.isnan(np.asarray(x))))[0]
#     
#     combined = np.transpose(np.array([pid, x, y, z, vx, vy, vz]))
#     combined_valid = combined[inds,:]
#     rho = sphdensity.calculateDensity(m,h,combined_valid[:,1:4])
#     outarray = np.ndarray((combined_valid.shape[0],10))
#     outarray[:,0:7] = combined_valid
#     outarray[:,7] = np.reshape(rho, (len(rho),))
#     outarray[:,8] = m
#     outarray[:,9] = h
#     fileheader = 'ID,X,Y,Z,Vx,Vy,Vz,rho,pmass,h'
#     np.savetxt(filepath_out, outarray, delimiter=',',header=fileheader,comments='')
#     
#     print(i)
#==============================================================================




