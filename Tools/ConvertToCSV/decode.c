/*
 * Builds a Python function to extract numerical data from a set of cached
 * data created by the Blender Particle engine.
 */
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdio.h>

/*
 * Entry point for the Python script. Decodes a frame of Particle data.
 *
 * Arguments:
 *    - filepath_in (string)  Path to the cache file.
 *    - bounds (float[6])     Maximum and minimum bounds to consider for
 *                            extraction. Format is
 *                            [xmin, xmax, ymin, ymax, zmin, zmax]. No value
 *                            uses whole domain.
 *    - vmax (float)          Maximum velocity allowed.
 *
 * Returns:
 *    - title (string)
 *    - ptype
 *    - n (int)               Number of particles in frame
 *    - d
 *    - pid (int[])           Array of particle ID's
 *    - x, y, z (float[])     x,y,z positions of particles
 *    - vx, vy, vz (float[])  x,y,z velocities of particles
 */
static PyObject* sph_decodeBlenderFile(PyObject* self, PyObject* args) {
    const char *filename;
    float vmax = 100000;
    float xmin;
    float xmax;
    float ymin;
    float ymax;
    float zmin;
    float zmax;
    PyObject* bounds = 0;
    FILE* infile;

    if (!PyArg_ParseTuple(args, "s|Of", &filename, &bounds, &vmax))
        return NULL;

    infile = fopen(filename, "rb");
    double vmax2 = vmax*vmax;
    if(bounds != 0) {
        xmin = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 0));
        xmax = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 1));
        ymin = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 2));
        ymax = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 3));
        zmin = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 4));
        zmax = (float)PyFloat_AsDouble(PyList_GetItem(bounds, 5));
    }

    //-> Decode the header of the file
    char title[8];
    unsigned int type;
    unsigned int npoints;
    unsigned int npoints_in=0;
    unsigned int dtypes;

    for(unsigned int i=0; i<8; i++) fread(&(title[i]), sizeof(title[i]),
        1, infile);

    fread(&type, 4, 1, infile);
    fread(&npoints, 4, 1, infile);
    fread(&dtypes, 4, 1, infile);

    unsigned int id;
    float x, y, z;
    float vx, vy, vz;
    PyObject* id_list = PyList_New(0);
    PyObject* x_list = PyList_New(0);
    PyObject* y_list = PyList_New(0);
    PyObject* z_list = PyList_New(0);
    PyObject* vx_list = PyList_New(0);
    PyObject* vy_list = PyList_New(0);
    PyObject* vz_list = PyList_New(0);

    //-> Decode particles
    int i = 0;
    while(!feof(infile)) {
        int valid = 1;
        fread(&id, 4, 1, infile);
        fread(&x, sizeof(x), 1, infile);
        fread(&y, sizeof(x), 1, infile);
        fread(&z, sizeof(x), 1, infile);
        fread(&vx, sizeof(x), 1, infile);
        fread(&vy, sizeof(x), 1, infile);
        fread(&vz, sizeof(x), 1, infile);
        // Check for validity
        float v2 = vx*vx + vy*vy + vz*vz;
        if(v2 > vmax2) valid = 0;
        if(bounds != 0) {
            if(x < xmin || x > xmax ||
               y < ymin || y > ymax ||
               z < zmin || z > zmax) valid = 0;
        }

        PyObject* py_x;
        PyObject* py_y;
        PyObject* py_z;
        PyObject* py_vx;
        PyObject* py_vy;
        PyObject* py_vz;
        PyObject* py_id;

        if(valid) {
            py_x = Py_BuildValue("f",x);
            py_y = Py_BuildValue("f",y);
            py_z = Py_BuildValue("f",z);
            py_vx = Py_BuildValue("f",vx);
            py_vy = Py_BuildValue("f",vy);
            py_vz = Py_BuildValue("f",vz);
            py_id = Py_BuildValue("i",id);

            PyList_Append(id_list, py_id);
            PyList_Append(x_list, py_x);
            PyList_Append(y_list, py_y);
            PyList_Append(z_list, py_z);
            PyList_Append(vx_list, py_vx);
            PyList_Append(vy_list, py_vy);
            PyList_Append(vz_list, py_vz);

            npoints_in++;
        } /*else {
            py_x = Py_BuildValue("f",0.0);
            py_y = Py_BuildValue("f",0.0);
            py_z = Py_BuildValue("f",0.0);
            py_vx = Py_BuildValue("f",0.0);
            py_vy = Py_BuildValue("f",0.0);
            py_vz = Py_BuildValue("f",0.0);
            py_id = Py_BuildValue("i",-1);
        }*/

        i++;
        if(i >= npoints)break;
    }



    return Py_BuildValue("siiiOOOOOOO", title, type, npoints_in, dtypes,
                         id_list, x_list, y_list, z_list,
                         vx_list, vy_list, vz_list);
}

/* Documentation strings */
static char module_docstring[] =
    "This module provides methods written in C for the VMPT program.";
static char decodeBlenderFile_docstring[] =
    "Decode a Blender particle cache file.";

/* Add all methods */
static PyMethodDef sphdecode_methods[] = {
    {"decodeBlenderFile", sph_decodeBlenderFile, METH_VARARGS, decodeBlenderFile_docstring},
    {NULL, NULL, 0, NULL}
};

/* Entry point for Python script */
PyMODINIT_FUNC initsphdecode(void) {
    PyObject *m = Py_InitModule3("sphdecode",
                    sphdecode_methods,
                   module_docstring);
    if(m == NULL)
        return;

    import_array();
}
