/*
 * Creates a Python module to calculate the density from a  Blender Particle
 * cache frame.
 */
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdio.h>



static PyObject* sphdensity_calculateDensity(PyObject* self, PyObject* args) {
    PyArrayObject* xyz;
    // Mass of particles
    float m;
    // SPH kernel distance
    float h;
    // Reference density
    float rho0;
    float h2;
    float A;
    // Number of particles
    int n;

    if (!PyArg_ParseTuple(args, "fffO!", &m, &h, &rho0, &PyArray_Type, &xyz))
        return NULL;

    if((int)PyArray_DIM(xyz, 1) != 3)
        return Py_BuildValue("i",0);

    double* data = (double*)PyArray_DATA(xyz);

    n = (int) PyArray_DIM(xyz, 0);
    PyObject* rho_list = PyList_New(n);

    h2 = h*h;
    // Smoothing kernel constant.
    A = 21.0/(256.0 * M_PI * h2*h);
             
    /*
     * A brute force method is used here to search for neighbours. A more 
     * sophisticated method should be implemented which uses binning structures
     * which store particles within bins and simply check for neighbouring bins.
     */

    for(int i=0; i<n; i++) {
        for(int j=i; j<n; j++) {
            float x1 = data[i*3 + 0];
            float y1 = data[i*3 + 1];
            float z1 = data[i*3 + 2];

            float x2 = data[j*3 + 0];
            float y2 = data[j*3 + 1];
            float z2 = data[j*3 + 2];

            float dx = x1-x2;
            float dy = y1-y2;
            float dz = z1-z2;

            float r2 = dx*dx + dy*dy + dz*dz;
            float q = sqrt(r2) / h;

            if(q < 2) {
                float B = pow(2 - q,4)*(1 + 2*q);
                float drho = m * A * B;

                PyObject* rho_i = PyList_GetItem(rho_list, i);
                PyObject* rho_j = PyList_GetItem(rho_list, j);
                float f_rho_i;
                float f_rho_j;
                // Check to see whether the i_th position has a density assigned.
                // If so, add rho_i to entry. If not, create entry.
                if(rho_i == 0) {
                    f_rho_i = drho;
                } else {
                    f_rho_i = drho + (float)PyFloat_AsDouble(rho_i);
                }
                
                // Same for rho_j
                if(rho_j == 0) {
                    f_rho_j = drho;
                } else {
                    f_rho_j = drho + (float)PyFloat_AsDouble(rho_j);
                }
                // Set density values
                PyObject* new_rho_i = Py_BuildValue("f", f_rho_i);
                PyObject* new_rho_j = Py_BuildValue("f", f_rho_j);
                PyList_SetItem(rho_list, i, new_rho_i);
                PyList_SetItem(rho_list, j, new_rho_j);
            }
        }
    }
    /*
     * NOTE:
     *
     * This part is not standard SPH practice. However, it is used by the
     * Particle engine when calculating density, so is used here to maintain
     * continuity.
     */
    for(int k=0; k<n; k++) {
        PyObject* rho_k = PyList_GetItem(rho_list, k);
        float f_rho_k = (float)PyFloat_AsDouble(rho_k);
        if(f_rho_k < rho0*0.9) {
            PyObject* new_rho_k = Py_BuildValue("f", rho0*0.9);
            PyList_SetItem(rho_list, k, new_rho_k);
        }
        if(f_rho_k > rho0*1.1) {
            PyObject* new_rho_k = Py_BuildValue("f", rho0*1.1);
            PyList_SetItem(rho_list, k, new_rho_k);            
        }
    }

    return Py_BuildValue("O",rho_list);
}

/* Documentation strings */
static char module_docstring[] =
    "This module provides methods written in C for the VMPT program.";
static char calculateDensity_docstring[] =
    "Decode a Blender particle cache file.";

/* Add all methods */
static PyMethodDef sphdensity_methods[] = {
    {"calculateDensity", sphdensity_calculateDensity, METH_VARARGS, calculateDensity_docstring},
    {NULL, NULL, 0, NULL}
};

/* Entry point for Python script */
PyMODINIT_FUNC initsphdensity(void) {
    PyObject *m = Py_InitModule3("sphdensity",
                    sphdensity_methods,
                   module_docstring);
    if(m == NULL)
        return;

    import_array();
}
