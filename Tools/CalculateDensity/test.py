import sphdensity
import numpy as np

A = np.genfromtxt('/home/dylan/Blender/NoSlipAnalysis/analysis/slip/2000.csv',delimiter=',',skip_header=1)

m = 0.0045
h = 0.037

rho = sphdensity.calculateDensity(m,h,A[:,1:4])

