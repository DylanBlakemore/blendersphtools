#include <math.h>

struct Point;
struct Grid;
struct Bin;

void buildGrid(const float* bounds, float h, unsigned int N_p, Grid* grid_out);
void destroyGrid(Grid* grid_in);
void buildPoint(float x_in, float y_in, float z_in, Point* p_out);
void getNeighbours(const Grid* grid_in, Point* P_out);

struct Point
{
    float x;
    float y;
    float z;        
};

void buildPoint(float x_in, float y_in, float z_in, Point3* p_out)
{
    p_out->x = x_in;
    p_out->y = y_in;
    p_out->z = z_in;
}

// =================================================================================== //
// =================================================================================== //

static struct Bin
{
	float x, y, z;
	float h;

	unsigned int Np;

	Point* P;
};

static void buildBin()
{

}

// =================================================================================== //
// =================================================================================== //

struct Grid
{
    float h;
    float xmin, ymin, zmin;
    float xmax, ymax, zmax;

    unsigned int Nx, Ny, Nz;
    unsigned int n;

    Bin* bins;
};

void buildGrid(const float* bounds, float h, unsigned int N, Grid* grid_out)
{
    grid_out->xmin = bounds[0]; 
    grid_out->xmax = bounds[1];
    grid_out->ymin = bounds[2];
    grid_out->ymax = bounds[3];
    grid_out->zmin = bounds[4];
    grid_out->zmax = bounds[5];

    grid_out->h = h;

    int nx = ceil((grid_out->xmax - grid_out->xmin) / h);
    int ny = ceil((grid_out->ymax - grid_out->ymin) / h);
    int nz = ceil((grid_out->zmax - grid_out->zmin) / h);
}

void addPointToGrid(const Point* P_in, Grid* grid_out)
{

}

void getNeighbours(const Grid* grid_in, Point* P_out)
{
        
}

