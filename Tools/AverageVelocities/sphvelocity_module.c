/*
 * Creates a Python module to calculate the density from a  Blender Particle
 * cache frame.
 */
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdio.h>

static PyObject* sphdensity_calculateVelocity(PyObject* self, PyObject* args) {
    PyArrayObject* xyz;
    PyArrayObject* varr;
    PyArrayObject* rhoarr;
    // Mass of particles
    float m;
    // SPH kernel distance
    float h;
    float h2;
    float A;
    // Number of particles
    int n;

    if (!PyArg_ParseTuple(args, "ffO!O!O!", &m, &h,
    		&PyArray_Type, &xyz, &PyArray_Type, &varr, &PyArray_Type, &rhoarr))
        return NULL;

    if((int)PyArray_DIM(xyz, 1) != 3)
        return Py_BuildValue("i",0);

    double* x_data = (double*)PyArray_DATA(xyz);
    double* v_data = (double*)PyArray_DATA(varr);
    double* rho_data = (double*)PyArray_DATA(rhoarr);

    n = (int) PyArray_DIM(xyz, 0);
    PyObject* vx_list = PyList_New(n);
    PyObject* vy_list = PyList_New(n);
    PyObject* vz_list = PyList_New(n);

    h2 = h*h;
    // Smoothing kernel constant.
    A = 21.0/(256.0 * M_PI * h2*h);
             
    /*
     * A brute force method is used here to search for neighbours. A more 
     * sophisticated method should be implemented which uses binning structures
     * which store particles within bins and simply check for neighbouring bins.
     */

    for(int i=0; i<n; i++) {
        for(int j=i; j<n; j++) {
        	/*
        	 * Determine distance between particles. For use in
        	 * weighting kernel.
        	 */
        	float x1 = x_data[i*3 + 0];
			float y1 = x_data[i*3 + 1];
			float z1 = x_data[i*3 + 2];

			float x2 = x_data[j*3 + 0];
			float y2 = x_data[j*3 + 1];
			float z2 = x_data[j*3 + 2];

			float dx = x1-x2;
			float dy = y1-y2;
			float dz = z1-z2;

			float r2 = dx*dx + dy*dy + dz*dz;
			float q = sqrt(r2) / h;
			
			/* 
			 * Condition for the two particles to be summed.
			 */
			if(q < 2) {
				float B = pow(2 - q,4)*(1 + 2*q);
				float rho_i = rho_data[i];
				float rho_j = rho_data[j];
				/*
				 * X-dimension
				 */
				PyObject* old_vx_i = PyList_GetItem(vx_list, i);
				PyObject* old_vx_j = PyList_GetItem(vx_list, j);
				
				float new_vx_i;
				float new_vx_j;
				
				if(old_vx_i == 0) 
					new_vx_i = v_data[j*3 + 0]*m*A*B/rho_j;
				else
					new_vx_i = (float)PyFloat_AsDouble(old_vx_i) + v_data[j*3 + 0]*m*A*B/rho_j;
				
				if(old_vx_j == 0) 
					new_vx_j = v_data[i*3 + 0]*m*A*B/rho_i;
				else
					new_vx_j = (float)PyFloat_AsDouble(old_vx_j) + v_data[i*3 + 0]*m*A*B/rho_i;
				
				
				PyObject* new_vx_i_py = Py_BuildValue("f", new_vx_i);
				PyObject* new_vx_j_py = Py_BuildValue("f", new_vx_j);
				PyList_SetItem(vx_list, i, new_vx_i_py);
				PyList_SetItem(vx_list, j, new_vx_j_py);
				
				/*
				 * Y-dimension
				 */
				PyObject* old_vy_i = PyList_GetItem(vy_list, i);
				PyObject* old_vy_j = PyList_GetItem(vy_list, j);
				
				float new_vy_i;
				float new_vy_j;
				
				if(old_vy_i == 0) 
					new_vy_i = v_data[j*3 + 1]*m*A*B/rho_j;
				else
					new_vy_i = (float)PyFloat_AsDouble(old_vy_i) + v_data[j*3 + 1]*m*A*B/rho_j;
				
				if(old_vy_j == 0) 
					new_vy_j = v_data[i*3 + 1]*m*A*B/rho_i;
				else
					new_vy_j = (float)PyFloat_AsDouble(old_vy_j) + v_data[i*3 + 1]*m*A*B/rho_i;
				
				
				PyObject* new_vy_i_py = Py_BuildValue("f", new_vy_i);
				PyObject* new_vy_j_py = Py_BuildValue("f", new_vy_j);
				PyList_SetItem(vy_list, i, new_vy_i_py);
				PyList_SetItem(vy_list, j, new_vy_j_py);
				/*
				 * Z-dimension
				 */
				PyObject* old_vz_i = PyList_GetItem(vz_list, i);
				PyObject* old_vz_j = PyList_GetItem(vz_list, j);
				
				float new_vz_i;
				float new_vz_j;
				
				if(old_vz_i == 0) 
					new_vz_i = v_data[j*3 + 2]*m*A*B/rho_j;
				else
					new_vz_i = (float)PyFloat_AsDouble(old_vz_i) + v_data[j*3 + 2]*m*A*B/rho_j;
				
				if(old_vz_j == 0) 
					new_vz_j = v_data[i*3 + 2]*m*A*B/rho_i;
				else
					new_vz_j = (float)PyFloat_AsDouble(old_vz_j) + v_data[i*3 + 2]*m*A*B/rho_i;
				
				
				PyObject* new_vz_i_py = Py_BuildValue("f", new_vz_i);
				PyObject* new_vz_j_py = Py_BuildValue("f", new_vz_j);
				PyList_SetItem(vz_list, i, new_vz_i_py);
				PyList_SetItem(vz_list, j, new_vz_j_py);
			}
        }
    }

    return Py_BuildValue("OOO",vx_list, vy_list, vz_list);
}

/* Documentation strings */
static char module_docstring[] =
    "This module provides a function to average the velocities of an SPH data set.";
static char calculateVelocity_docstring[] =
    "Average the velocities of an SPH data set with the same kernel used by Blender.";

/* Add all methods */
static PyMethodDef sphvelocity_methods[] = {
    {"calculateVelocity", sphdensity_calculateVelocity, METH_VARARGS, calculateVelocity_docstring},
    {NULL, NULL, 0, NULL}
};

/* Entry point for Python script */
PyMODINIT_FUNC initsphvelocity(void) {
    PyObject *m = Py_InitModule3("sphvelocity",
                    sphvelocity_methods,
                   module_docstring);
    if(m == NULL)
        return;

    import_array();
}
