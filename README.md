### Description ###

This library is a collection of tools used to analyse the output from the Blender Particle system.

The functions are mainly written in C to improve performance. 

### How do I get set up? ###

To build the libraries:

1. Open the terminal and navigate to the folder containing the setup.py file.

2. Use the command `python setup.py build` to run the setup file.

3. A file called _libraryname.so_ will be created in _./build/lib.linux-x86_64-2.7_ (example is _sphdensity.so_).

4. Move this file to the folder containing your Python scripts, and use the `import libraryname` command to import it into the script.
 
### Dependencies ###

These libraries have only been tested on Ubuntu 16.04 using Python 2.7.
They may work on other systems, but there is no guarantee.
